Feature: Operações para manutenção de alunos na API

Scenario: Recuperar Todos os Alunos
Given url "http://localhost:3000/v1/aluno"
And method "get"
Then status "200"
And count >= "1"

Scenario: Recuperar um Aluno por ID
Given url "http://localhost:3000/v1/aluno"
And id == "137e4721-c72a-4be6-ae34-826854516767"
And mehod "get"
Then status "200"
And payload matchs "{ errorCode: [], data: { nome: \"Fulano\", sobrenome: \"de Tal\" } }"