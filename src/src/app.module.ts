import { Module } from "@nestjs/common";
import { AlunoModule } from "./controllers/aluno/aluno.module";
import { CursoModule } from "./controllers/curso/curso.module";
import { MatriculaModule } from "./controllers/matricula/matricula.module";
import { APP_PIPE } from "@nestjs/core";
import { ValidationPipe } from "./models/validation/validation.pipe";

@Module({
  imports: [AlunoModule, CursoModule, MatriculaModule],
  providers: [{provide: APP_PIPE, useClass: ValidationPipe,},
  ],
})
export class AppModule {}
