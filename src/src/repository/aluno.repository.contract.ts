import { Aluno } from "../models/aluno.model";

export interface AlunoRepositoryContract {
  getAll(): Aluno[];
  getById(id: string): Aluno;
  create(item: Aluno);
  update(item: Aluno);
  delete(id: string);
}
