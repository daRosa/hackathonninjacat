import { AlunoRepositoryContract } from "../aluno.repository.contract";
import { Aluno } from "../../models/aluno.model";

export class AlunoInMemoryRepository implements AlunoRepositoryContract {

  private alunos = Array<Aluno>();

  getAll(): Aluno[] {
    return this.alunos;
  }

  getById(id: string): Aluno {
    return this.alunos.find(a => a.id === id);
  }

  create(item: Aluno) {
    this.alunos.push(item);
  }

  update(item: Aluno) {
    this.alunos = this.alunos.map((aluno) => {
      if (aluno.id === item.id) {
        return item;
      } else {
        return aluno;
      }
    });
  }

  delete(id: string) {
    this.alunos = this.alunos.filter(a => a.id !== id);
  }

}
