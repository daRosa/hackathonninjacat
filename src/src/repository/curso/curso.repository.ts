import { CursoRepositoryContract } from "./curso.repository.contract";
import { Curso } from "../../models/curso.model";
import { MongoClient } from "mongodb";

export class CursoRepository implements CursoRepositoryContract {

  private cursos = Array<Curso>();

  async getAll(): Promise<Curso[]> {
    const mongo = await MongoClient.connect("mongodb://127.0.0.1:27017");
    const payload = await mongo.db("reach-engine").collection("cursos").find().toArray();

    const result = payload.map(item => Object.assign(new Curso(), item));
    return result as Curso[];
  }

  getById(id: string): Curso {
    return this.cursos.find(x => x.id === id);
  }

  create(item: Curso) {
    this.cursos.push(item);
  }

  update(item: Curso) {
    this.cursos = this.cursos.map((curso) => {
        if (curso.id === item.id) {
            return item;
        } else { return curso; }
    });
  }

  delete(id: string) {
    this.cursos = this.cursos.filter(curso => curso.id !== id);
  }

}
