import { Curso } from "../../models/curso.model";

export interface CursoRepositoryContract {
  getAll(): Promise<Curso[]>;
  getById(id: string): Curso;
  create(item: Curso);
  update(item: Curso);
  delete(id: string);
}
