import { MatriculaRepositoryContract } from "../matricula.repository.contract";
import { Matricula } from "../../models/matricula.model";

export class MatriculaInMemoryRepository implements MatriculaRepositoryContract {

  private matriculas = Array<Matricula>();

  create(item: Matricula) {
    this.matriculas.push(item);
  }

  delete(idAluno: string, idCurso: string) {
    this.matriculas = this.matriculas.filter(m => m.idAluno !== idAluno && m.idCurso !== idCurso);
  }

  getStudentsByCourseId(idCurso: string) {
    return this.matriculas.filter(m => m.idCurso === idCurso);
  }

  getCoursesByStudentId(idAluno: string) {
    return this.matriculas.filter(m => m.idAluno === idAluno);
  }

}
