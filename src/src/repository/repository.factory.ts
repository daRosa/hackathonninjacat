import { AlunoRepositoryContract } from "./aluno.repository.contract";
import { AlunoInMemoryRepository } from "./aluno/aluno.inmemory.repository";
import { CursoRepositoryContract } from "./curso/curso.repository.contract";
import { CursoRepository } from "./curso/curso.repository";
import { MatriculaRepositoryContract } from "./matricula.repository.contract";
import { MatriculaInMemoryRepository } from "./matricula/matricula.inmemory.repository";
import { Injectable } from "@nestjs/common";

@Injectable()
export class RepositoryFactory {
  private static alunoRepository: AlunoRepositoryContract;
  private static cursoRepository: CursoRepositoryContract;
  private static matriculaRepository: MatriculaRepositoryContract;
  /**
   * Obtém o respositório de alunos
   */
  public getAlunoRepository(): AlunoRepositoryContract {
    if (!RepositoryFactory.alunoRepository) {
      RepositoryFactory.alunoRepository = new AlunoInMemoryRepository();
    }
    return RepositoryFactory.alunoRepository;
  }

  /**
   * Obtém o respositório de cursos
   */
  public getCursoRepository(): CursoRepositoryContract {
    if (!RepositoryFactory.cursoRepository) {
      RepositoryFactory.cursoRepository = new CursoRepository();
    }
    return RepositoryFactory.cursoRepository;
  }

  /**
   * Obtém o respositório de matrículas
   */
  public getMatriculaRepository(): MatriculaRepositoryContract {
    if (!RepositoryFactory.matriculaRepository) {
      RepositoryFactory.matriculaRepository = new MatriculaInMemoryRepository();
    }
    return RepositoryFactory.matriculaRepository;
  }
}
