import { Matricula } from "../models/matricula.model";
import { Aluno } from "../models/aluno.model";
import { Curso } from "../models/curso.model";

export interface MatriculaRepositoryContract {
  create(item: Matricula);
  delete(idAluno: string, idCurso: string);
  getStudentsByCourseId(idCurso: string): Matricula[];
  getCoursesByStudentId(idAluno: string): Matricula[];
}
