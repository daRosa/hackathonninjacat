import { IsEmail, IsNotEmpty, IsString, IsDate, IsEmpty, IsDateString, IsUUID, IsOptional } from "class-validator";

export class Aluno {

  @IsUUID()
  @IsOptional()
  public id: string;

  @IsString()
  public nome: string;

  @IsString()
  public sobrenome: string;

  @IsString()
  public cpf: string;

  @IsString()
  public dataNascimento: Date;
}
