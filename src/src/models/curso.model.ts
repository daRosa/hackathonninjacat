import { Modalidade } from "./modalidade.enum";
import { IsEmpty, IsString, IsNumber, IsInstance, IsEnum, IsNotEmpty, IsOptional, IsUUID } from "class-validator";
import { Optional } from "@nestjs/common";

export class Curso {
  @IsUUID()
  @IsOptional()
  public id: string;

  @IsString()
  public nome: string;

  @IsNumber()
  public cargaHoraria: number;

  @IsEnum(Modalidade)
  @IsString()
  public modalidade: Modalidade;

  @IsNumber()
  @IsOptional()
  public capacidade: number;

  @IsString()
  @IsOptional()
  public dataInicio: Date;
}
