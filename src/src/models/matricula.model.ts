export class Matricula {
  public id: string;
  public idAluno: string;
  public idCurso: string;
  public data: Date;
}
