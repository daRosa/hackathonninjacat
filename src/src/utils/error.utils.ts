import { ErrorCode } from "../services/errorcode";

export class ErrorUtils extends Error {
    constructor(code: ErrorCode) {
        super();
        this.errorCode = code;
    }

    public errorCode: ErrorCode;

}
