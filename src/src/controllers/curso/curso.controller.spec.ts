import * as request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import { RetornoPadrao } from "../../services/retornopadrao";
import { ErrorCode } from "../../services/errorcode";
import { Curso } from "../../models/curso.model";
import { CursoModule } from "./curso.module";
import { CursoService } from "./curso.service";
import { ErrorUtils } from "../../utils/error.utils";

const mockCursos = [
  { id: "123456", nome: "Curso de Python 3.7" } as Curso,
  { id: "999999", nome: "Curso de Node 10.5" } as Curso,
];

// Mocking
class MockCursoService {
  getAll() {
    return mockCursos;
  }

  getById(id: string) {
    return mockCursos.find(curso => curso.id === id);
  }

  create(curso: Curso) {
    mockCursos.push(curso);
  }

  update(curso: Curso) {
    const existente = this.getById(curso.id);
    if (!existente) { throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    mockCursos.map(cursoExistente => {
      return (cursoExistente.id === curso.id) ? curso : cursoExistente;
    });
  }
}

describe("CursoController", () => {
  let app: INestApplication;

  beforeEach(async () => {
    const mod: TestingModule = await Test.createTestingModule({
      imports: [CursoModule],
    })
    .overrideProvider(CursoService)
    .useValue(new MockCursoService())
    .compile();

    app = mod.createNestApplication();
    await app.init();
  });

  describe("/GET curso", () => {

    it("Get todos - ✅", () => {
      return request(app.getHttpServer())
        .get("/curso/")
        .expect(200)
        .expect({
          errorCodes: [],
          data: mockCursos,
        } as RetornoPadrao);
    });

    it("Get Por Id - ✅", () => {
      return request(app.getHttpServer())
        .get(`/curso/${999999}`)
        .expect(200)
        .expect({
          errorCodes: [],
          data: mockCursos[1],
        } as RetornoPadrao);
    });

  });

  describe("/POST e /PUT curso", () => {

    it("Inserir item - ✅", () => {
      return request(app.getHttpServer())
        .post("/curso")
        .send({
          id: "321321",
          nome: "Curso Temporário XYZ",
          cargaHoraria: 200,
        } as Curso)
        .expect(200)
        .expect({
          errorCodes: [],
          data: null,
        } as RetornoPadrao);
    });

    it("Update item - ✅", () => {
      return request(app.getHttpServer())
        .put("/curso")
        .send({
          id: "999999",
          nome: "Curso Modificado XYZ",
          cargaHoraria: 100,
        } as Curso)
        .expect(200)
        .expect({
          errorCodes: [],
          data: null,
        } as RetornoPadrao);
    });

    it("Update item - id inexistente ⛔️", () => {
      return request(app.getHttpServer())
        .put("/curso")
        .send({
          id: "444444",
          nome: "Curso Modificado XYZ",
          cargaHoraria: 100,
        } as Curso)
        .expect(500)
        .expect({
          errorCodes: [ErrorCode.IdCursoInvalido],
          data: null,
        } as RetornoPadrao);
    });

  });
});
