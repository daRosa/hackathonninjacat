import { Controller, Get, Res, Post, Body, Param, Put, Delete, Inject } from "@nestjs/common";
import { CursoService } from "./curso.service";
import { RetornoPadraoService } from "../../services/retornopadrao.service";
import { ErrorCode } from "../../services/errorcode";
import { Response } from "express";
import { Curso } from "../../models/curso.model";
import { ApiUseTags } from "@nestjs/swagger";

@ApiUseTags("curso")
@Controller("curso")
export class CursoController {
  constructor(
    @Inject("RetornoPadraoService") private readonly provedorRetorno: RetornoPadraoService,
    private readonly cursoService: CursoService) {}

  @Get()
  async get(@Res() response: Response) {
    let payload: any;
    try {
      payload = await this.cursoService.getAll();
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);

  }

  @Get(":id")
  getById(@Param("id") id, @Res() response: Response): any {
    let payload: any;
    try {
      payload = this.cursoService.getById(id);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }
    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Post()
  create(@Body() cursoDto, @Res() response: Response): void {
    let payload: any;
    try {
      payload = this.cursoService.create(cursoDto);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Put()
  update(@Body() cursoDto, @Res() response: Response): void {
    let payload: any;
    try {
      payload = this.cursoService.update(cursoDto);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Delete(":id")
  delete(@Param("id") id, @Res() response: Response): void {
    let payload: any;
    try {
      payload = this.cursoService.delete(id);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

}
