import * as moment from "moment";
import { Test, TestingModule } from "@nestjs/testing";
import { CursoService } from "./curso.service";
import { Curso } from "../../models/curso.model";
import { Modalidade } from "../../models/modalidade.enum";
import { CursoRepositoryContract } from "../../repository/curso/curso.repository.contract";

describe("Testes Unitários Curso Service", () => {
  let cursoService: CursoService;

  beforeEach(async () => {
    const mod: TestingModule = await Test.createTestingModule({
      imports: [
        CursoService,
      ],
    })
    .compile();

    cursoService = mod.get<CursoService>("CursoService");
  });

  it("/GET curso - ✅", async () => {
    return expect(
      await cursoService.getAll(),
    ).not.toHaveLength(0);
  });

  it("/POST curso - ✅", () => {
    return expect(
      cursoService.create({
        nome: "Curso /POST custo teste",
        modalidade: Modalidade.Presencial,
        cargaHoraria: 200,
        dataInicio: moment("29/05/2019 08:00:00", "DD/MM/YYYY HH:mm:SS").utc().toDate(),
        capacidade: 20,
      } as Curso),
    ).toBeUndefined();
  });

});
