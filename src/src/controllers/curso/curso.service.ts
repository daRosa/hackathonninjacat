import { Injectable } from "@nestjs/common";
import { RepositoryFactory } from "../../repository/repository.factory";
import { Curso } from "../../models/curso.model";
import { ErrorUtils } from "../../utils/error.utils";
import { ErrorCode } from "../../services/errorcode";

@Injectable()
export class CursoService {
  public factory: RepositoryFactory;

  constructor() {
    this.factory = new RepositoryFactory();
  }

  getById(id: string) {
    return this.factory.getCursoRepository().getById(id);
  }

  update(curso: Curso) {
    const cursoNotInDatabase = !this.getById(curso.id);

    if (cursoNotInDatabase) { throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    this.factory.getCursoRepository().update(curso);
  }

  getAll() {
    return this.factory.getCursoRepository().getAll();
  }

  create(curso: Curso) {
    this.factory.getCursoRepository().create(curso);
  }

  delete(id: string) {
    const cursoNotInDatabase = !this.getById(id);

    if (cursoNotInDatabase) { throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    this.factory.getCursoRepository().delete(id);
  }

  // TODO
  getQuantityOfStudentsEnrolled(id: string) {
    return 0;
  }

  isCourseEnrollableNow(id: string) {
    const curso = this.getById(id);
    const deadline: number = curso.dataInicio.getDate() - 1;
    return deadline > Date.now();
  }

  isCourseFull(id: string) {
    const curso = this.getById(id);
    return curso.capacidade <= this.getQuantityOfStudentsEnrolled(id);
  }

  // TODO
  isStudentEnrolledinCourse(idAluno: string, idCurso) {
    return false;
  }

}
