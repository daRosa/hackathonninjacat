import { Module } from "@nestjs/common";
import { CursoController } from "./curso.controller";
import { CursoService } from "./curso.service";
import { RetornoPadraoService } from "../../services/retornopadrao.service";

@Module({
  controllers: [CursoController],
  providers: [CursoService, RetornoPadraoService],
})
export class CursoModule {

}
