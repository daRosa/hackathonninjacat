import { Body, Controller, Post, Delete, Inject, Res } from "@nestjs/common";
import { MatriculaService } from "./matricula.service";
import { Response } from "express";
import { RetornoPadraoService } from "../../services/retornopadrao.service";
import { ErrorCode } from "../../services/errorcode";
import { ApiUseTags } from "@nestjs/swagger";

@ApiUseTags("matricula")
@Controller("matricula")
export class MatriculaController {
  constructor(
    @Inject("RetornoPadraoService") private readonly provedorRetorno: RetornoPadraoService,
    private readonly matriculaService: MatriculaService) { }

  @Post()
  create(@Body() matriculaDto, @Res() response: Response) {
    let payload = null;

    try {
      payload = this.matriculaService.create(matriculaDto);
    } catch (error) {
      this.provedorRetorno.appendError(ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Delete()
  delete(@Res() response: Response) {
    let payload = null;

    try {
      payload = null;
    } catch (error) {
      this.provedorRetorno.appendError(ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }
}
