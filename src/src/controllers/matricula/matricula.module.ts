import { Module } from "@nestjs/common";
import { MatriculaController } from "./matricula.controller";
import { MatriculaService } from "./matricula.service";
import { RetornoPadraoService } from "../../services/retornopadrao.service";

@Module({
  controllers: [MatriculaController],
  providers: [MatriculaService, RetornoPadraoService],
})
export class MatriculaModule {

}
