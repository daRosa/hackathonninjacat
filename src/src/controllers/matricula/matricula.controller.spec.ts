import * as request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import { RetornoPadrao } from "../../services/retornopadrao";
import { ErrorCode } from "../../services/errorcode";
import { Matricula } from "../../models/matricula.model";
import { MatriculaService } from "./matricula.service";
import { MatriculaModule } from "./matricula.module";

const mockMatriculas = [
  { id: "234234", idAluno: "999999", idCurso: "888888" } as Matricula,
  { id: "234234", idAluno: "XYZKWS", idCurso: "787878" } as Matricula,
];

// Mocking
class MockMatriculaService {

  create(matriculaDto: any) {
    mockMatriculas.push({
      id: String(Math.random()),
      idAluno: matriculaDto.idAluno,
      idCurso: matriculaDto.idCurso,
    } as Matricula);
  }

  delete(idAlunoParam: string, idCursoParam: string) {
    let indiceEncontrado = -1;
    const matriculaEncontrada =
      mockMatriculas.find((matricula, index) => {
        const encontrado = (matricula.idAluno === idAlunoParam && matricula.idCurso === idCursoParam);
        if (encontrado) { indiceEncontrado = index; }

        return encontrado;
      });

    if (!matriculaEncontrada) { throw Error("Matrícula não encontrada"); }

    mockMatriculas.splice(indiceEncontrado, 1);
  }
}

describe("Matricula Controller", () => {
  let app: INestApplication;

  beforeEach(async () => {
    const mod: TestingModule = await Test.createTestingModule({
      imports: [MatriculaModule],
    })
    .overrideProvider(MatriculaService)
    .useValue(new MockMatriculaService())
    .compile();

    app = mod.createNestApplication();
    await app.init();
  });

  describe("/POST e /DELETE Matricula", () => {

    it("Inserir item - ✅", () => {
      return request(app.getHttpServer())
        .post("/matricula")
        .send({
          idAluno: "999999",
          idCurso: "343434",
        })
        .expect(200)
        .expect({
          errorCodes: [],
          data: null,
        } as RetornoPadrao);
    });

    it("Delete item - ✅", () => {
      return request(app.getHttpServer())
        .delete("/matricula")
        .send({
          idAluno: "XYZKWS",
          idCurso: "787878",
        })
        .expect(200)
        .expect({
          errorCodes: [],
          data: null,
        } as RetornoPadrao);
    });

    // it("Delete item - id aluno e curso inválidos ⛔️", () => {
    //   return request(app.getHttpServer())
    //     .put("/matricula")
    //     .send({
    //       idAluno: "000001",
    //       idCurso: "100000",
    //     })
    //     .expect(500)
    //     .expect({
    //       errorCodes: [ErrorCode.IdCursoInvalido],
    //       data: null,
    //     } as RetornoPadrao);
    // });

  });
});
