import { Injectable, Inject } from "@nestjs/common";
import { RepositoryFactory } from "../../repository/repository.factory";
import { Matricula } from "../../models/matricula.model";
import { CursoService } from "../curso/curso.service";
import { Modalidade } from "../../models/modalidade.enum";
import { ErrorUtils } from "../../utils/error.utils";
import { ErrorCode } from "../../services/errorcode";
import { AlunoService } from "../aluno/aluno.service";

@Injectable()
export class MatriculaService {
  private readonly factory: RepositoryFactory;

  constructor() {
    this.factory = new RepositoryFactory();
  }

  create(matriculaDto: any) {
    const idCurso = matriculaDto.idCurso;
    const idAluno = matriculaDto.idAluno;

    const curso = this.factory.getCursoRepository().getById(idCurso);
    const cursoService: CursoService = new CursoService();

    if (!curso) {throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    if (curso.modalidade === Modalidade.Presencial) {
      if (!cursoService.isCourseEnrollableNow(idCurso)) {throw new ErrorUtils(ErrorCode.CursoEmAndamento); }
      if (!cursoService.isCourseFull(idCurso)) {throw new ErrorUtils(ErrorCode.CursoLotado); }
    }

    const estudanteService: AlunoService = new AlunoService();
    const aluno = estudanteService.getById(idAluno);

    if (!aluno) {throw new ErrorUtils(ErrorCode.IdAlunoInvalido); }

    if (this.isStudentEnrolledinCourse(idAluno, idCurso)) {throw new ErrorUtils(ErrorCode.MatriculaJaExiste); }

    const matricula: Matricula = {
      id: "", // TODO: Add GUID
      idAluno,
      idCurso,
      data: new Date(),
    };
    this.factory.getMatriculaRepository().create(matricula);
  }

  delete(idAluno: string, idCurso: string) {
    this.factory.getMatriculaRepository().delete(idAluno, idCurso);
  }

  getStudentsInCourse(idCurso: string) {
    return this.factory.getMatriculaRepository().getStudentsByCourseId(idCurso);
  }

  getCoursesOfAStudent(idAluno: string) {
    return this.factory.getMatriculaRepository().getCoursesByStudentId(idAluno);
  }

  isStudentEnrolledinCourse(idAluno: string, idCurso: string) {
    return this.getCoursesOfAStudent(idAluno).find(c => c.id === idCurso) !== undefined;
  }
}
