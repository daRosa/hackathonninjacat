import { Controller, Get, Post, Body, Param, Put, Delete, Inject, Res } from "@nestjs/common";
import { AlunoService } from "./aluno.service";
import { ErrorCode } from "../../services/errorcode";
import { Response } from "express";
import { RetornoPadraoService } from "../../services/retornopadrao.service";
import { ApiUseTags } from "@nestjs/swagger";

@ApiUseTags("aluno")
@Controller("aluno")
export class AlunoController {
  constructor(
    @Inject("RetornoPadraoService") private readonly provedorRetorno: RetornoPadraoService,
    private readonly alunoService: AlunoService) {}

  @Get()
  getAll(@Res() response: Response): any {
    let payload: any;
    try {
      payload = this.alunoService.getAll();
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Get(":id")
  getById(@Param("id") id, @Res() response: Response): any {
    let payload: any;
    try {
      payload = this.alunoService.getById(id);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Post()
  create(@Body() alunoDto: Aluno, @Res() response: Response): any {
    let payload: any;
    try {
      payload = this.alunoService.create(alunoDto);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Put()
  update(@Body() alunoDto, @Res() response: Response): void {
    let payload: any;
    try {
      payload = this.alunoService.update(alunoDto);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }

  @Delete(":id")
  delete(@Param("id") id, @Res() response: Response): void {
    let payload: any;
    try {
      payload = this.alunoService.delete(id);
    } catch (error) {
      this.provedorRetorno.appendError(error.errorCode ? error.errorCode : ErrorCode.Indefinido);
    }

    this.provedorRetorno.hasErrors() ?
      this.provedorRetorno.reject(response) :
      this.provedorRetorno.success(payload, response);
  }
}
