import { Module } from "@nestjs/common";
import { AlunoController } from "./aluno.controller";
import { AlunoService } from "./aluno.service";
import { RetornoPadraoService } from "../../services/retornopadrao.service";

@Module({
  controllers: [AlunoController],
  providers: [AlunoService, RetornoPadraoService],
})
export class AlunoModule {

}
