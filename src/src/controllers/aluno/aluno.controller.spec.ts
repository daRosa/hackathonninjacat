import * as request from "supertest";
import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import { RetornoPadrao } from "../../services/retornopadrao";
import { ErrorCode } from "../../services/errorcode";
import { Aluno } from "../../models/aluno.model";
import { AlunoModule } from "./aluno.module";
import { AlunoService } from "./aluno.service";
import { ErrorUtils } from "../../utils/error.utils";

const mockAlunos = [
  { id: "123456", nome: "Fulano", sobrenome: "de Tal", cpf: "111.111.111-11"} as Aluno,
  { id: "999999", nome: "Beltrana", sobrenome: "da Silva", cpf: "999.999.333-44"} as Aluno,
];

// Mocking
class MockAlunoService {
  getAll() {
    return mockAlunos;
  }

  getById(id: string) {
    return mockAlunos.find(aluno => aluno.id === id);
  }

  create(aluno: Aluno) {
    mockAlunos.push(aluno);
  }

  update(aluno: Aluno) {
    const existente = this.getById(aluno.id);
    if (!existente) { throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    mockAlunos.map(alunoExistente => {
      return (alunoExistente.id === aluno.id) ? aluno : alunoExistente;
    });
  }
}

describe("Aluno Controller", () => {
  let app: INestApplication;

  beforeEach(async () => {
    const mod: TestingModule = await Test.createTestingModule({
      imports: [AlunoModule],
    })
    .overrideProvider(AlunoService)
    .useValue(new MockAlunoService())
    .compile();

    app = mod.createNestApplication();
    await app.init();
  });

  describe("/GET aluno", () => {

    it("Get todos - ✅", () => {
      return request(app.getHttpServer())
        .get("/aluno")
        .expect(200)
        .expect({
          errorCodes: [],
          data: mockAlunos,
        } as RetornoPadrao);
    });

    it("Get Por Id - ✅", () => {
      return request(app.getHttpServer())
        .get(`/aluno/${999999}`)
        .expect(200)
        .expect({
          errorCodes: [],
          data: mockAlunos[1] ,
        } as RetornoPadrao);
    });
  });

  describe("/POST e /PUT aluno", () => {

    it("Inserir item - ✅", () => {
      return request(app.getHttpServer())
        .post("/aluno")
        .send({
          id: "321321",
          nome: "Aluno Novo",
          sobrenome: "Pomposo",
          cpf: "345.345.345-12",
          dataNascimento: new Date(),
        } as Aluno)
        .expect(200)
        .expect({
          errorCodes: [],
          data: null,
        } as RetornoPadrao);
    });

    it("Update item - ✅", () => {
      return request(app.getHttpServer())
        .put("/aluno")
        .send({
          id: "999999",
          nome: "Aluno com Nome",
          sobrenome: "Modificado",
          cpf: "345.345.345-12",
          dataNascimento: new Date(),
        } as Aluno)
        .expect(200)
        .expect({
          errorCodes: [],
          data: null,
        } as RetornoPadrao);
    });

    it("Update item - id inexistente ⛔️", () => {
      return request(app.getHttpServer())
        .put("/aluno")
        .send({
          id: "444444",
          nome: "Aluno Inexistente",
          sobrenome: "Inválido",
          cpf: "345.345.345-12",
          dataNascimento: new Date(),
        } as Aluno)
        .expect(500)
        .expect({
          errorCodes: [ErrorCode.IdCursoInvalido],
          data: null,
        } as RetornoPadrao);
    });

  });
});
