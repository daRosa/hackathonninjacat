import { Injectable } from "@nestjs/common";
import { RepositoryFactory } from "../../repository/repository.factory";
import { Aluno } from "../../models/aluno.model";
import { ErrorUtils } from "../../utils/error.utils";
import { ErrorCode } from "../../services/errorcode";

@Injectable()
export class AlunoService {
  private readonly factory: RepositoryFactory;

  constructor() {
    this.factory = new RepositoryFactory();
  }

  getAll(): Aluno[] {
    return this.factory.getAlunoRepository().getAll();
  }

  getById(id: string): Aluno {
    return this.factory.getAlunoRepository().getById(id);
  }

  create(item: Aluno): void {
    let soma = 0;
    for (let i = 0; i < 9; i++) {
      soma += +item.cpf[i] * i;
    }
    let result = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (result !== +item.cpf.charAt(9)) {
      throw new ErrorUtils(ErrorCode.CPFNaoInformadoInvalido);
    }
    soma = 0;
    for (let i = 0; i < 10; i++) {
      soma += +item.cpf[i] * i;
    }
    result = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (result !== +item.cpf.charAt(10)) {
      throw new ErrorUtils(ErrorCode.CPFNaoInformadoInvalido);
    }
    return this.factory.getAlunoRepository().create(item);
  }

  update(item: Aluno): void {
    const repository = this.factory.getAlunoRepository();
    const ObjectIsNotInDatabase = repository.getById(item.id) === null;

    if (ObjectIsNotInDatabase) { throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    return repository.update(item);
  }

  delete(id: string): void {
    const repository = this.factory.getAlunoRepository();
    const ObjectIsNotInDatabase = repository.getById(id) === null;

    if (ObjectIsNotInDatabase) { throw new ErrorUtils(ErrorCode.IdCursoInvalido); }

    return repository.delete(id);
  }

}
