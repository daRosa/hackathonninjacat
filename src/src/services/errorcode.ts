export enum ErrorCode {
  Indefinido = 901,

  NomeSobrenomeNaoInformados = 101,
  CPFNaoInformadoInvalido = 102,
  DataNascimentoNaoInforamada = 103,
  IdAlunoNaoInfomado = 111,

  NomeCursoNaoInformado = 201,
  CargaHorariaNaoInformada = 202,
  IdCursoNaoInformado = 211,

  DataNaoInformada = 301,
  IdAlunoInvalido = 311,
  IdCursoInvalido = 312,

  CursoEmAndamento = 401,
  CursoLotado = 402,

  MatriculaJaExiste = 501,
}
