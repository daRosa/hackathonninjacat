import { Injectable, Res } from "@nestjs/common";
import { RetornoPadrao } from "./retornopadrao";
import { ErrorCode } from "./errorcode";
import { Response } from "express";

@Injectable()
export class RetornoPadraoService {
  private errorCodes = Array<ErrorCode>();

  /**
   * Informa se o provider atual tem erros acumulados para retorno. 🤔
   */
  public hasErrors(): boolean {
    return this.errorCodes.length > 0;
  }

  /**
   * Retorno de sucesso. ✅
   * Ignora os erros acumulados
   * @param data dados a serem enviados via payload.
   */
  public success(info: any, response: Response) {
    response
      .status(200)
      .send({
        errorCodes: [],
        data: info ? info : null,
      } as RetornoPadrao);
  }

  /**
   * Acrescenta um novo erro provider. ❗️
   * Deve ser chamado antes do reject.
   * @param errorCode código de erro a ser adicionado.
   */
  public appendError(errorCode: ErrorCode): RetornoPadraoService {
    this.errorCodes.push(errorCode);
    return this;
  }

  /**
   * Retorno padrão com os erros acumulados. ⛔️
   */
  public reject(response: Response) {
    response
      .status(500)
      .send({
        errorCodes: this.errorCodes,
        data: null,
      } as RetornoPadrao);
  }
}
