import { ErrorCode } from "./errorcode";

export class RetornoPadrao {
  public errorCodes: ErrorCode[];
  public data: any;
}
