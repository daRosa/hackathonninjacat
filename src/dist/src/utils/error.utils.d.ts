import { ErrorCode } from "../services/errorcode";
export declare class ErrorUtils extends Error {
    constructor(code: ErrorCode);
    errorCode: ErrorCode;
}
