"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ErrorUtils extends Error {
    constructor(code) {
        super();
        this.errorCode = code;
    }
}
exports.ErrorUtils = ErrorUtils;
//# sourceMappingURL=error.utils.js.map