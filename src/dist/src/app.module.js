"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const aluno_module_1 = require("./controllers/aluno/aluno.module");
const curso_module_1 = require("./controllers/curso/curso.module");
const matricula_module_1 = require("./controllers/matricula/matricula.module");
const core_1 = require("@nestjs/core");
const validation_pipe_1 = require("./models/validation/validation.pipe");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [aluno_module_1.AlunoModule, curso_module_1.CursoModule, matricula_module_1.MatriculaModule],
        providers: [{ provide: core_1.APP_PIPE, useClass: validation_pipe_1.ValidationPipe, },
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map