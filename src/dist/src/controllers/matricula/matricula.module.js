"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const matricula_controller_1 = require("./matricula.controller");
const matricula_service_1 = require("./matricula.service");
const retornopadrao_service_1 = require("../../services/retornopadrao.service");
let MatriculaModule = class MatriculaModule {
};
MatriculaModule = __decorate([
    common_1.Module({
        controllers: [matricula_controller_1.MatriculaController],
        providers: [matricula_service_1.MatriculaService, retornopadrao_service_1.RetornoPadraoService],
    })
], MatriculaModule);
exports.MatriculaModule = MatriculaModule;
//# sourceMappingURL=matricula.module.js.map