"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const repository_factory_1 = require("../../repository/repository.factory");
const curso_service_1 = require("../curso/curso.service");
const modalidade_enum_1 = require("../../models/modalidade.enum");
let MatriculaService = class MatriculaService {
    constructor() {
        this.factory = new repository_factory_1.RepositoryFactory();
    }
    create(matriculaDto) {
        const idCurso = matriculaDto.idCurso;
        const idAluno = matriculaDto.idAluno;
        const curso = this.factory.getCursoRepository().getById(idCurso);
        const cursoService = new curso_service_1.CursoService();
        if (curso.modalidade === modalidade_enum_1.Modalidade.Presencial) {
            if (!cursoService.isCourseEnrollableNow(idCurso)) {
                throw new Error("Matricula fora do prazo");
            }
            if (!cursoService.isCourseFull(idCurso)) {
                throw new Error("Curso está cheio");
            }
        }
        if (this.isStudentEnrolledinCourse(idAluno, idCurso)) {
            throw new Error("Aluno já está matriculado nesse curso");
        }
        const matricula = {
            id: "",
            idAluno,
            idCurso,
            data: new Date(),
        };
        this.factory.getMatriculaRepository().create(matricula);
    }
    delete(idAluno, idCurso) {
        this.factory.getMatriculaRepository().delete(idAluno, idCurso);
    }
    getStudentsInCourse(idCurso) {
        return this.factory.getMatriculaRepository().getStudentsByCourseId(idCurso);
    }
    getCoursesOfAStudent(idAluno) {
        return this.factory.getMatriculaRepository().getCoursesByStudentId(idAluno);
    }
    isStudentEnrolledinCourse(idAluno, idCurso) {
        return this.getCoursesOfAStudent(idAluno).find(c => c.id === idCurso) !== undefined;
    }
};
MatriculaService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], MatriculaService);
exports.MatriculaService = MatriculaService;
//# sourceMappingURL=matricula.service.js.map