"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("supertest");
const testing_1 = require("@nestjs/testing");
const matricula_service_1 = require("./matricula.service");
const matricula_module_1 = require("./matricula.module");
const mockMatriculas = [
    { id: "234234", idAluno: "999999", idCurso: "888888" },
    { id: "234234", idAluno: "XYZKWS", idCurso: "787878" },
];
class MockMatriculaService {
    create(matriculaDto) {
        mockMatriculas.push({
            id: String(Math.random()),
            idAluno: matriculaDto.idAluno,
            idCurso: matriculaDto.idCurso,
        });
    }
    delete(idAlunoParam, idCursoParam) {
        let indiceEncontrado = -1;
        const matriculaEncontrada = mockMatriculas.find((matricula, index) => {
            const encontrado = (matricula.idAluno === idAlunoParam && matricula.idCurso === idCursoParam);
            if (encontrado) {
                indiceEncontrado = index;
            }
            return encontrado;
        });
        if (!matriculaEncontrada) {
            throw Error("Matrícula não encontrada");
        }
        mockMatriculas.splice(indiceEncontrado, 1);
    }
}
describe("Matricula Controller", () => {
    let app;
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        const mod = yield testing_1.Test.createTestingModule({
            imports: [matricula_module_1.MatriculaModule],
        })
            .overrideProvider(matricula_service_1.MatriculaService)
            .useValue(new MockMatriculaService())
            .compile();
        app = mod.createNestApplication();
        yield app.init();
    }));
    describe("/POST e /DELETE Matricula", () => {
        it("Inserir item - ✅", () => {
            return request(app.getHttpServer())
                .post("/matricula")
                .send({
                idAluno: "999999",
                idCurso: "343434",
            })
                .expect(200)
                .expect({
                errorCodes: [],
                data: null,
            });
        });
        it("Delete item - ✅", () => {
            return request(app.getHttpServer())
                .delete("/matricula")
                .send({
                idAluno: "XYZKWS",
                idCurso: "787878",
            })
                .expect(200)
                .expect({
                errorCodes: [],
                data: null,
            });
        });
    });
});
//# sourceMappingURL=matricula.controller.spec.js.map