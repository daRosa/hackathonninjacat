import { MatriculaService } from "./matricula.service";
import { Response } from "express";
import { RetornoPadraoService } from "../../services/retornopadrao.service";
export declare class MatriculaController {
    private readonly provedorRetorno;
    private readonly matriculaService;
    constructor(provedorRetorno: RetornoPadraoService, matriculaService: MatriculaService);
    create(matriculaDto: any, response: Response): void;
    delete(response: Response): void;
}
