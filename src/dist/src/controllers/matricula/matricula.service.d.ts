import { Matricula } from "../../models/matricula.model";
export declare class MatriculaService {
    private readonly factory;
    constructor();
    create(matriculaDto: any): void;
    delete(idAluno: string, idCurso: string): void;
    getStudentsInCourse(idCurso: string): Matricula[];
    getCoursesOfAStudent(idAluno: string): Matricula[];
    isStudentEnrolledinCourse(idAluno: string, idCurso: string): boolean;
}
