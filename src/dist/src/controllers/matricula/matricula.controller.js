"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const matricula_service_1 = require("./matricula.service");
const retornopadrao_service_1 = require("../../services/retornopadrao.service");
const errorcode_1 = require("../../services/errorcode");
const swagger_1 = require("@nestjs/swagger");
let MatriculaController = class MatriculaController {
    constructor(provedorRetorno, matriculaService) {
        this.provedorRetorno = provedorRetorno;
        this.matriculaService = matriculaService;
    }
    create(matriculaDto, response) {
        let payload = null;
        try {
            payload = this.matriculaService.create(matriculaDto);
        }
        catch (error) {
            this.provedorRetorno.appendError(errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    delete(response) {
        let payload = null;
        try {
            payload = null;
        }
        catch (error) {
            this.provedorRetorno.appendError(errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], MatriculaController.prototype, "create", null);
__decorate([
    common_1.Delete(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], MatriculaController.prototype, "delete", null);
MatriculaController = __decorate([
    swagger_1.ApiUseTags("matricula"),
    common_1.Controller("matricula"),
    __param(0, common_1.Inject("RetornoPadraoService")),
    __metadata("design:paramtypes", [retornopadrao_service_1.RetornoPadraoService,
        matricula_service_1.MatriculaService])
], MatriculaController);
exports.MatriculaController = MatriculaController;
//# sourceMappingURL=matricula.controller.js.map