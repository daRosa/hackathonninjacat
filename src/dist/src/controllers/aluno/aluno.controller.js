"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const aluno_service_1 = require("./aluno.service");
const errorcode_1 = require("../../services/errorcode");
const retornopadrao_service_1 = require("../../services/retornopadrao.service");
const aluno_model_1 = require("../../models/aluno.model");
const swagger_1 = require("@nestjs/swagger");
let AlunoController = class AlunoController {
    constructor(provedorRetorno, alunoService) {
        this.provedorRetorno = provedorRetorno;
        this.alunoService = alunoService;
    }
    getAll(response) {
        let payload;
        try {
            payload = this.alunoService.getAll();
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    getById(id, response) {
        let payload;
        try {
            payload = this.alunoService.getById(id);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    create(alunoDto, response) {
        let payload;
        try {
            payload = this.alunoService.create(alunoDto);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    update(alunoDto, response) {
        let payload;
        try {
            payload = this.alunoService.update(alunoDto);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    delete(id, response) {
        let payload;
        try {
            payload = this.alunoService.delete(id);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], AlunoController.prototype, "getAll", null);
__decorate([
    common_1.Get(":id"),
    __param(0, common_1.Param("id")), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Object)
], AlunoController.prototype, "getById", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [aluno_model_1.Aluno, Object]),
    __metadata("design:returntype", Object)
], AlunoController.prototype, "create", null);
__decorate([
    common_1.Put(),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], AlunoController.prototype, "update", null);
__decorate([
    common_1.Delete(":id"),
    __param(0, common_1.Param("id")), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], AlunoController.prototype, "delete", null);
AlunoController = __decorate([
    swagger_1.ApiUseTags("aluno"),
    common_1.Controller("aluno"),
    __param(0, common_1.Inject("RetornoPadraoService")),
    __metadata("design:paramtypes", [retornopadrao_service_1.RetornoPadraoService,
        aluno_service_1.AlunoService])
], AlunoController);
exports.AlunoController = AlunoController;
//# sourceMappingURL=aluno.controller.js.map