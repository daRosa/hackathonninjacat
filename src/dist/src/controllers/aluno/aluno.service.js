"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const repository_factory_1 = require("../../repository/repository.factory");
const error_utils_1 = require("../../utils/error.utils");
const errorcode_1 = require("../../services/errorcode");
let AlunoService = class AlunoService {
    constructor() {
        this.factory = new repository_factory_1.RepositoryFactory();
    }
    getAll() {
        throw new Error("72");
        return this.factory.getAlunoRepository().getAll();
    }
    getById(id) {
        return this.factory.getAlunoRepository().getById(id);
    }
    create(item) {
        let soma = 0;
        for (let i = 0; i < 9; i++) {
            soma += +item.cpf[i] * i;
        }
        let result = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (result !== +item.cpf.charAt(9)) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.CPFNaoInformadoInvalido);
        }
        soma = 0;
        for (let i = 0; i < 10; i++) {
            soma += +item.cpf[i] * i;
        }
        result = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (result !== +item.cpf.charAt(10)) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.CPFNaoInformadoInvalido);
        }
        return this.factory.getAlunoRepository().create(item);
    }
    update(item) {
        const repository = this.factory.getAlunoRepository();
        const ObjectIsNotInDatabase = repository.getById(item.id) === null;
        if (ObjectIsNotInDatabase) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.IdCursoInvalido);
        }
        return repository.update(item);
    }
    delete(id) {
        const repository = this.factory.getAlunoRepository();
        const ObjectIsNotInDatabase = repository.getById(id) === null;
        if (ObjectIsNotInDatabase) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.IdCursoInvalido);
        }
        return repository.delete(id);
    }
};
AlunoService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], AlunoService);
exports.AlunoService = AlunoService;
//# sourceMappingURL=aluno.service.js.map