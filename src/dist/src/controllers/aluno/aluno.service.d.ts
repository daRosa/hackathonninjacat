import { Aluno } from "../../models/aluno.model";
export declare class AlunoService {
    private readonly factory;
    constructor();
    getAll(): Aluno[];
    getById(id: string): Aluno;
    create(item: Aluno): void;
    update(item: Aluno): void;
    delete(id: string): void;
}
