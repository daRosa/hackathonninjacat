import { AlunoService } from "./aluno.service";
import { Response } from "express";
import { RetornoPadraoService } from "../../services/retornopadrao.service";
import { Aluno } from "../../models/aluno.model";
export declare class AlunoController {
    private readonly provedorRetorno;
    private readonly alunoService;
    constructor(provedorRetorno: RetornoPadraoService, alunoService: AlunoService);
    getAll(response: Response): any;
    getById(id: any, response: Response): any;
    create(alunoDto: Aluno, response: Response): any;
    update(alunoDto: any, response: Response): void;
    delete(id: any, response: Response): void;
}
