"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("supertest");
const testing_1 = require("@nestjs/testing");
const errorcode_1 = require("../../services/errorcode");
const aluno_module_1 = require("./aluno.module");
const aluno_service_1 = require("./aluno.service");
const error_utils_1 = require("../../utils/error.utils");
const mockAlunos = [
    { id: "123456", nome: "Fulano", sobrenome: "de Tal", cpf: "111.111.111-11" },
    { id: "999999", nome: "Beltrana", sobrenome: "da Silva", cpf: "999.999.333-44" },
];
class MockAlunoService {
    getAll() {
        return mockAlunos;
    }
    getById(id) {
        return mockAlunos.find(aluno => aluno.id === id);
    }
    create(aluno) {
        mockAlunos.push(aluno);
    }
    update(aluno) {
        const existente = this.getById(aluno.id);
        if (!existente) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.IdCursoInvalido);
        }
        mockAlunos.map(alunoExistente => {
            return (alunoExistente.id === aluno.id) ? aluno : alunoExistente;
        });
    }
}
describe("Aluno Controller", () => {
    let app;
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        const mod = yield testing_1.Test.createTestingModule({
            imports: [aluno_module_1.AlunoModule],
        })
            .overrideProvider(aluno_service_1.AlunoService)
            .useValue(new MockAlunoService())
            .compile();
        app = mod.createNestApplication();
        yield app.init();
    }));
    describe("/GET aluno", () => {
        it("Get todos - ✅", () => {
            return request(app.getHttpServer())
                .get("/aluno")
                .expect(200)
                .expect({
                errorCodes: [],
                data: mockAlunos,
            });
        });
        it("Get Por Id - ✅", () => {
            return request(app.getHttpServer())
                .get(`/aluno/${999999}`)
                .expect(200)
                .expect({
                errorCodes: [],
                data: mockAlunos[1],
            });
        });
    });
    describe("/POST e /PUT aluno", () => {
        it("Inserir item - ✅", () => {
            return request(app.getHttpServer())
                .post("/aluno")
                .send({
                id: "321321",
                nome: "Aluno Novo",
                sobrenome: "Pomposo",
                cpf: "345.345.345-12",
                dataNascimento: new Date(),
            })
                .expect(200)
                .expect({
                errorCodes: [],
                data: null,
            });
        });
        it("Update item - ✅", () => {
            return request(app.getHttpServer())
                .put("/aluno")
                .send({
                id: "999999",
                nome: "Aluno com Nome",
                sobrenome: "Modificado",
                cpf: "345.345.345-12",
                dataNascimento: new Date(),
            })
                .expect(200)
                .expect({
                errorCodes: [],
                data: null,
            });
        });
        it("Update item - id inexistente ⛔️", () => {
            return request(app.getHttpServer())
                .put("/aluno")
                .send({
                id: "444444",
                nome: "Aluno Inexistente",
                sobrenome: "Inválido",
                cpf: "345.345.345-12",
                dataNascimento: new Date(),
            })
                .expect(500)
                .expect({
                errorCodes: [errorcode_1.ErrorCode.IdCursoInvalido],
                data: null,
            });
        });
    });
});
//# sourceMappingURL=aluno.controller.spec.js.map