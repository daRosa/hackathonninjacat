import { Curso } from "../../models/curso.model";
export declare class CursoService {
    private readonly factory;
    constructor();
    getById(id: string): Curso;
    update(curso: Curso): void;
    getAll(): Curso[];
    create(curso: Curso): void;
    delete(id: string): void;
    getQuantityOfStudentsEnrolled(id: string): number;
    isCourseEnrollableNow(id: string): boolean;
    isCourseFull(id: string): boolean;
    isStudentEnrolledinCourse(idAluno: string, idCurso: any): boolean;
}
