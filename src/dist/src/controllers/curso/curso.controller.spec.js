"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("supertest");
const testing_1 = require("@nestjs/testing");
const errorcode_1 = require("../../services/errorcode");
const curso_module_1 = require("./curso.module");
const curso_service_1 = require("./curso.service");
const error_utils_1 = require("../../utils/error.utils");
const mockCursos = [
    { id: "123456", nome: "Curso de Python 3.7" },
    { id: "999999", nome: "Curso de Node 10.5" },
];
class MockCursoService {
    getAll() {
        return mockCursos;
    }
    getById(id) {
        return mockCursos.find(curso => curso.id === id);
    }
    create(curso) {
        mockCursos.push(curso);
    }
    update(curso) {
        const existente = this.getById(curso.id);
        if (!existente) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.IdCursoInvalido);
        }
        mockCursos.map(cursoExistente => {
            return (cursoExistente.id === curso.id) ? curso : cursoExistente;
        });
    }
}
describe("CursoController", () => {
    let app;
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        const mod = yield testing_1.Test.createTestingModule({
            imports: [curso_module_1.CursoModule],
        })
            .overrideProvider(curso_service_1.CursoService)
            .useValue(new MockCursoService())
            .compile();
        app = mod.createNestApplication();
        yield app.init();
    }));
    describe("/GET curso", () => {
        it("Get todos - ✅", () => {
            return request(app.getHttpServer())
                .get("/curso/")
                .expect(200)
                .expect({
                errorCodes: [],
                data: mockCursos,
            });
        });
        it("Get Por Id - ✅", () => {
            return request(app.getHttpServer())
                .get(`/curso/${999999}`)
                .expect(200)
                .expect({
                errorCodes: [],
                data: mockCursos[1],
            });
        });
    });
    describe("/POST e /PUT curso", () => {
        it("Inserir item - ✅", () => {
            return request(app.getHttpServer())
                .post("/curso")
                .send({
                id: "321321",
                nome: "Curso Temporário XYZ",
                cargaHoraria: 200,
            })
                .expect(200)
                .expect({
                errorCodes: [],
                data: null,
            });
        });
        it("Update item - ✅", () => {
            return request(app.getHttpServer())
                .put("/curso")
                .send({
                id: "999999",
                nome: "Curso Modificado XYZ",
                cargaHoraria: 100,
            })
                .expect(200)
                .expect({
                errorCodes: [],
                data: null,
            });
        });
        it("Update item - id inexistente ⛔️", () => {
            return request(app.getHttpServer())
                .put("/curso")
                .send({
                id: "444444",
                nome: "Curso Modificado XYZ",
                cargaHoraria: 100,
            })
                .expect(500)
                .expect({
                errorCodes: [errorcode_1.ErrorCode.IdCursoInvalido],
                data: null,
            });
        });
    });
});
//# sourceMappingURL=curso.controller.spec.js.map