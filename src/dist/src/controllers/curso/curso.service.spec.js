"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@nestjs/testing");
const curso_service_1 = require("./curso.service");
const moment = require("moment");
const modalidade_enum_1 = require("../../models/modalidade.enum");
describe("Testes Unitários Curso Service", () => {
    let cursoService;
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        const mod = yield testing_1.Test.createTestingModule({
            providers: [curso_service_1.CursoService],
        })
            .compile();
        cursoService = mod.get("CursoService");
    }));
    it("/POST curso - ✅", () => {
        return expect(cursoService.create({
            nome: "Curso /POST custo teste",
            modalidade: modalidade_enum_1.Modalidade.Presencial,
            cargaHoraria: 200,
            dataInicio: moment("29/05/2019 08:00:00", "DD/MM/YYYY HH:mm:SS").utc().toDate(),
            capacidade: 20,
        })).toHaveBeenCalled();
    });
});
//# sourceMappingURL=curso.service.spec.js.map