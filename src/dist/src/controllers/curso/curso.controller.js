"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const curso_service_1 = require("./curso.service");
const retornopadrao_service_1 = require("../../services/retornopadrao.service");
const errorcode_1 = require("../../services/errorcode");
const swagger_1 = require("@nestjs/swagger");
let CursoController = class CursoController {
    constructor(provedorRetorno, cursoService) {
        this.provedorRetorno = provedorRetorno;
        this.cursoService = cursoService;
    }
    get(response) {
        let payload;
        try {
            payload = this.cursoService.getAll();
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    getById(id, response) {
        let payload;
        try {
            payload = this.cursoService.getById(id);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    create(cursoDto, response) {
        let payload;
        try {
            payload = this.cursoService.create(cursoDto);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    update(cursoDto, response) {
        let payload;
        try {
            payload = this.cursoService.update(cursoDto);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
    delete(id, response) {
        let payload;
        try {
            payload = this.cursoService.delete(id);
        }
        catch (error) {
            this.provedorRetorno.appendError(error.errorCode ? error.errorCode : errorcode_1.ErrorCode.Indefinido);
        }
        this.provedorRetorno.hasErrors() ?
            this.provedorRetorno.reject(response) :
            this.provedorRetorno.success(payload, response);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], CursoController.prototype, "get", null);
__decorate([
    common_1.Get(":id"),
    __param(0, common_1.Param("id")), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Object)
], CursoController.prototype, "getById", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], CursoController.prototype, "create", null);
__decorate([
    common_1.Put(),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], CursoController.prototype, "update", null);
__decorate([
    common_1.Delete(":id"),
    __param(0, common_1.Param("id")), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], CursoController.prototype, "delete", null);
CursoController = __decorate([
    swagger_1.ApiUseTags("curso"),
    common_1.Controller("curso"),
    __param(0, common_1.Inject("RetornoPadraoService")),
    __metadata("design:paramtypes", [retornopadrao_service_1.RetornoPadraoService,
        curso_service_1.CursoService])
], CursoController);
exports.CursoController = CursoController;
//# sourceMappingURL=curso.controller.js.map