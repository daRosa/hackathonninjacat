import { CursoService } from "./curso.service";
import { RetornoPadraoService } from "../../services/retornopadrao.service";
import { Response } from "express";
export declare class CursoController {
    private readonly provedorRetorno;
    private readonly cursoService;
    constructor(provedorRetorno: RetornoPadraoService, cursoService: CursoService);
    get(response: Response): any;
    getById(id: any, response: Response): any;
    create(cursoDto: any, response: Response): void;
    update(cursoDto: any, response: Response): void;
    delete(id: any, response: Response): void;
}
