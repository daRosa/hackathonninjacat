"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const repository_factory_1 = require("../../repository/repository.factory");
const error_utils_1 = require("../../utils/error.utils");
const errorcode_1 = require("../../services/errorcode");
let CursoService = class CursoService {
    constructor() {
        this.factory = new repository_factory_1.RepositoryFactory();
    }
    getById(id) {
        return this.factory.getCursoRepository().getById(id);
    }
    update(curso) {
        const cursoNotInDatabase = !this.getById(curso.id);
        if (cursoNotInDatabase) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.IdCursoInvalido);
        }
        this.factory.getCursoRepository().update(curso);
    }
    getAll() {
        return this.factory.getCursoRepository().getAll();
    }
    create(curso) {
        this.factory.getCursoRepository().create(curso);
    }
    delete(id) {
        const cursoNotInDatabase = !this.getById(id);
        if (cursoNotInDatabase) {
            throw new error_utils_1.ErrorUtils(errorcode_1.ErrorCode.IdCursoInvalido);
        }
        this.factory.getCursoRepository().delete(id);
    }
    getQuantityOfStudentsEnrolled(id) {
        return 0;
    }
    isCourseEnrollableNow(id) {
        const curso = this.getById(id);
        const deadline = curso.dataInicio.getDate() - 1;
        return deadline > Date.now();
    }
    isCourseFull(id) {
        const curso = this.getById(id);
        return curso.capacidade <= this.getQuantityOfStudentsEnrolled(id);
    }
    isStudentEnrolledinCourse(idAluno, idCurso) {
        return false;
    }
};
CursoService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], CursoService);
exports.CursoService = CursoService;
//# sourceMappingURL=curso.service.js.map