import { Modalidade } from "./modalidade.enum";
export declare class Curso {
    id: string;
    nome: string;
    cargaHoraria: number;
    modalidade: Modalidade;
    capacidade: number;
    dataInicio: Date;
}
