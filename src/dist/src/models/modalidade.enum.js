"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Modalidade;
(function (Modalidade) {
    Modalidade[Modalidade["EAD"] = 0] = "EAD";
    Modalidade[Modalidade["Presencial"] = 1] = "Presencial";
})(Modalidade = exports.Modalidade || (exports.Modalidade = {}));
//# sourceMappingURL=modalidade.enum.js.map