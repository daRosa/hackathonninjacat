export declare class Aluno {
    id: string;
    nome: string;
    sobrenome: string;
    cpf: string;
    dataNascimento: Date;
}
