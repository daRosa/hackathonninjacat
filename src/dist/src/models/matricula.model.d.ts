export declare class Matricula {
    id: string;
    idAluno: string;
    idCurso: string;
    data: Date;
}
