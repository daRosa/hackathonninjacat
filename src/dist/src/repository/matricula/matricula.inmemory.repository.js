"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MatriculaInMemoryRepository {
    constructor() {
        this.matriculas = Array();
    }
    create(item) {
        this.matriculas.push(item);
    }
    delete(idAluno, idCurso) {
        this.matriculas = this.matriculas.filter(m => m.idAluno !== idAluno && m.idCurso !== idCurso);
    }
    getStudentsByCourseId(idCurso) {
        return this.matriculas.filter(m => m.idCurso === idCurso);
    }
    getCoursesByStudentId(idAluno) {
        return this.matriculas.filter(m => m.idAluno === idAluno);
    }
}
exports.MatriculaInMemoryRepository = MatriculaInMemoryRepository;
//# sourceMappingURL=matricula.inmemory.repository.js.map