import { MatriculaRepositoryContract } from "../matricula.repository.contract";
import { Matricula } from "../../models/matricula.model";
export declare class MatriculaInMemoryRepository implements MatriculaRepositoryContract {
    private matriculas;
    create(item: Matricula): void;
    delete(idAluno: string, idCurso: string): void;
    getStudentsByCourseId(idCurso: string): Matricula[];
    getCoursesByStudentId(idAluno: string): Matricula[];
}
