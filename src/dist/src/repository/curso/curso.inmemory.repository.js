"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CursoInMemoryRepository {
    constructor() {
        this.cursos = Array();
    }
    getAll() {
        return this.cursos;
    }
    getById(id) {
        return this.cursos.find(x => x.id === id);
    }
    create(item) {
        this.cursos.push(item);
    }
    update(item) {
        this.cursos = this.cursos.map((curso) => {
            if (curso.id === item.id) {
                return item;
            }
            else {
                return curso;
            }
        });
    }
    delete(id) {
        this.cursos = this.cursos.filter(curso => curso.id !== id);
    }
}
exports.CursoInMemoryRepository = CursoInMemoryRepository;
//# sourceMappingURL=curso.inmemory.repository.js.map