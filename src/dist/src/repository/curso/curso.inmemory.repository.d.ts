import { CursoRepositoryContract } from "../curso.repository.contract";
import { Curso } from "../../models/curso.model";
export declare class CursoInMemoryRepository implements CursoRepositoryContract {
    private cursos;
    getAll(): Curso[];
    getById(id: string): Curso;
    create(item: Curso): void;
    update(item: Curso): void;
    delete(id: string): void;
}
