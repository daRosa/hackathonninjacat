import { Matricula } from "../models/matricula.model";
export interface MatriculaRepositoryContract {
    create(item: Matricula): any;
    delete(idAluno: string, idCurso: string): any;
    getStudentsByCourseId(idCurso: string): Matricula[];
    getCoursesByStudentId(idAluno: string): Matricula[];
}
