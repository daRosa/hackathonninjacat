import { Curso } from "../models/curso.model";
export interface CursoRepositoryContract {
    getAll(): Curso[];
    getById(id: string): Curso;
    create(item: Curso): any;
    update(item: Curso): any;
    delete(id: string): any;
}
