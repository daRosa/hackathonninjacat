"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AlunoInMemoryRepository {
    constructor() {
        this.alunos = Array();
    }
    getAll() {
        return this.alunos;
    }
    getById(id) {
        return this.alunos.find(a => a.id === id);
    }
    create(item) {
        this.alunos.push(item);
    }
    update(item) {
        this.alunos = this.alunos.map((aluno) => {
            if (aluno.id === item.id) {
                return item;
            }
            else {
                return aluno;
            }
        });
    }
    delete(id) {
        this.alunos = this.alunos.filter(a => a.id !== id);
    }
}
exports.AlunoInMemoryRepository = AlunoInMemoryRepository;
//# sourceMappingURL=aluno.inmemory.repository.js.map