import { AlunoRepositoryContract } from "../aluno.repository.contract";
import { Aluno } from "../../models/aluno.model";
export declare class AlunoInMemoryRepository implements AlunoRepositoryContract {
    private alunos;
    getAll(): Aluno[];
    getById(id: string): Aluno;
    create(item: Aluno): void;
    update(item: Aluno): void;
    delete(id: string): void;
}
