import { Aluno } from "../models/aluno.model";
export interface AlunoRepositoryContract {
    getAll(): Aluno[];
    getById(id: string): Aluno;
    create(item: Aluno): any;
    update(item: Aluno): any;
    delete(id: string): any;
}
