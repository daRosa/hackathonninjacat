import { AlunoRepositoryContract } from "./aluno.repository.contract";
import { CursoRepositoryContract } from "./curso.repository.contract";
import { MatriculaRepositoryContract } from "./matricula.repository.contract";
export declare class RepositoryFactory {
    private static alunoRepository;
    private static cursoRepository;
    private static matriculaRepository;
    getAlunoRepository(): AlunoRepositoryContract;
    getCursoRepository(): CursoRepositoryContract;
    getMatriculaRepository(): MatriculaRepositoryContract;
}
