"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aluno_inmemory_repository_1 = require("./aluno/aluno.inmemory.repository");
const curso_inmemory_repository_1 = require("./curso/curso.inmemory.repository");
const matricula_inmemory_repository_1 = require("./matricula/matricula.inmemory.repository");
class RepositoryFactory {
    getAlunoRepository() {
        if (!RepositoryFactory.alunoRepository) {
            RepositoryFactory.alunoRepository = new aluno_inmemory_repository_1.AlunoInMemoryRepository();
        }
        return RepositoryFactory.alunoRepository;
    }
    getCursoRepository() {
        if (!RepositoryFactory.cursoRepository) {
            RepositoryFactory.cursoRepository = new curso_inmemory_repository_1.CursoInMemoryRepository();
        }
        return RepositoryFactory.cursoRepository;
    }
    getMatriculaRepository() {
        if (!RepositoryFactory.matriculaRepository) {
            RepositoryFactory.matriculaRepository = new matricula_inmemory_repository_1.MatriculaInMemoryRepository();
        }
        return RepositoryFactory.matriculaRepository;
    }
}
exports.RepositoryFactory = RepositoryFactory;
//# sourceMappingURL=repository.factory.js.map