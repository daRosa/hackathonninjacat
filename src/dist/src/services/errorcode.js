"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorCode;
(function (ErrorCode) {
    ErrorCode[ErrorCode["Indefinido"] = 901] = "Indefinido";
    ErrorCode[ErrorCode["NomeSobrenomeNaoInformados"] = 101] = "NomeSobrenomeNaoInformados";
    ErrorCode[ErrorCode["CPFNaoInformadoInvalido"] = 102] = "CPFNaoInformadoInvalido";
    ErrorCode[ErrorCode["DataNascimentoNaoInforamada"] = 103] = "DataNascimentoNaoInforamada";
    ErrorCode[ErrorCode["IDAlunoNaoInfomado"] = 111] = "IDAlunoNaoInfomado";
    ErrorCode[ErrorCode["NomeCursoNaoInformado"] = 201] = "NomeCursoNaoInformado";
    ErrorCode[ErrorCode["CargaHorariaNaoInformada"] = 202] = "CargaHorariaNaoInformada";
    ErrorCode[ErrorCode["IdCursoNaoInformado"] = 211] = "IdCursoNaoInformado";
    ErrorCode[ErrorCode["DataNaoInformada"] = 301] = "DataNaoInformada";
    ErrorCode[ErrorCode["IdAlunoInvalido"] = 311] = "IdAlunoInvalido";
    ErrorCode[ErrorCode["IdCursoInvalido"] = 312] = "IdCursoInvalido";
})(ErrorCode = exports.ErrorCode || (exports.ErrorCode = {}));
//# sourceMappingURL=errorcode.js.map