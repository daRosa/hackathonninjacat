import { ErrorCode } from "./errorcode";
import { Response } from "express";
export declare class RetornoPadraoService {
    private errorCodes;
    hasErrors(): boolean;
    success(info: any, response: Response): void;
    appendError(errorCode: ErrorCode): RetornoPadraoService;
    reject(response: Response): void;
}
