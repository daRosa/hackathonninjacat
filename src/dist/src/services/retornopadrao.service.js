"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
let RetornoPadraoService = class RetornoPadraoService {
    constructor() {
        this.errorCodes = Array();
    }
    hasErrors() {
        return this.errorCodes.length > 0;
    }
    success(info, response) {
        response
            .status(200)
            .send({
            errorCodes: [],
            data: info ? info : null,
        });
    }
    appendError(errorCode) {
        this.errorCodes.push(errorCode);
        return this;
    }
    reject(response) {
        response
            .status(500)
            .send({
            errorCodes: this.errorCodes,
            data: null,
        });
    }
};
RetornoPadraoService = __decorate([
    common_1.Injectable()
], RetornoPadraoService);
exports.RetornoPadraoService = RetornoPadraoService;
//# sourceMappingURL=retornopadrao.service.js.map